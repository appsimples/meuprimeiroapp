package com.senec.outsmart.meuprimeiroapp.View

import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.senec.outsmart.meuprimeiroapp.Model.PriorityType
import com.senec.outsmart.meuprimeiroapp.R
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class DetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        fillFields()
        toolbarTitle.text = "DETALHES DA ATIVIDADE"

    }

    private fun fillFields() {
        val priority = intent.getStringExtra("priority")
        descriptionTodo.text = intent.getStringExtra("description")
        titleToDo.text = intent.getStringExtra("title")
        priorityToDo.text = priority
        setPriorityTextColor(PriorityType.valueOf(priority))
    }

    private fun setPriorityTextColor(priorityText: PriorityType) {
        when (priorityText) {
            PriorityType.HIGH -> priorityToDo.setTextColor(ContextCompat.getColor(this, R.color.red))
            PriorityType.MEDIUM -> priorityToDo.setTextColor(ContextCompat.getColor(this, R.color.orange))
            else -> priorityToDo.setTextColor(ContextCompat.getColor(this, R.color.green))
        }
    }
}
