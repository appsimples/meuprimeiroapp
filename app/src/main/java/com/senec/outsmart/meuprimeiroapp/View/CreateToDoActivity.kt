package com.senec.outsmart.meuprimeiroapp.View

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.RadioButton
import com.senec.outsmart.meuprimeiroapp.Model.PriorityType
import com.senec.outsmart.meuprimeiroapp.R
import kotlinx.android.synthetic.main.activity_create_to_do.*
import kotlinx.android.synthetic.main.toolbar.*

class CreateToDoActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_to_do)
        setActionsToButton()

        toolbarTitle.text = "ADICIONAR ATIVIDADE"
    }

    private fun setActionsToButton() {
        btnCancel.setOnClickListener { finish() }
        btnSave.setOnClickListener {
            insertToDo()
        }
    }

    private fun insertToDo() {
        if (!(radioGroup.checkedRadioButtonId == -1 || titleEditText.text.toString().isEmpty())) {
            val intent = Intent()
            intent.putExtra("title", titleEditText.text.toString())
            intent.putExtra("description", descriptionEditText.text.toString())
            intent.putExtra("priority", getPriority())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun getPriority(): String {
        val radioButtonID = radioGroup.checkedRadioButtonId
        val radioButton = radioGroup.findViewById<RadioButton>(radioButtonID)

        return when (radioButton.text) {
            getString(R.string.prioridade_alta) -> PriorityType.HIGH.toString()
            getString(R.string.prioridade_media) -> PriorityType.MEDIUM.toString()
            else -> PriorityType.LOW.toString()
        }
    }
}
