package com.senec.outsmart.meuprimeiroapp.Controller

import com.senec.outsmart.meuprimeiroapp.Model.PriorityType
import com.senec.outsmart.meuprimeiroapp.Model.ToDoModel

class ToDoController {

    fun createToDoModel(title: String, priority: PriorityType, description: String): ToDoModel {
        return ToDoModel(title = title, priority = priority).apply { this.description = description }
    }

    fun createMockToDoModel(): ToDoModel = ToDoModel(title = "Apresentação da Senec ", priority = PriorityType.HIGH).apply { this.description = "Apresentar a atividade da Senec" }
}