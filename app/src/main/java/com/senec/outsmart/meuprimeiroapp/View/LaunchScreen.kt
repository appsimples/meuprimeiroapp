package com.senec.outsmart.meuprimeiroapp.View

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.senec.outsmart.meuprimeiroapp.R

class LaunchScreen : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_screen)
        goToToDoListActivity()

    }

    private fun goToToDoListActivity() {
        val handler = Handler()
        handler.postDelayed({
            finish()
            startActivity(Intent(this, ToDoListActivity::class.java))
        }, 500)
    }
}
