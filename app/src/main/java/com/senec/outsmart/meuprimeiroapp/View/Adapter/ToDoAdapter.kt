package com.senec.outsmart.meuprimeiroapp.View.Adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.senec.outsmart.meuprimeiroapp.Model.PriorityType
import com.senec.outsmart.meuprimeiroapp.Model.ToDoModel
import com.senec.outsmart.meuprimeiroapp.R
import com.senec.outsmart.meuprimeiroapp.View.DetailActivity
import kotlinx.android.synthetic.main.item_to_do_list.view.*

class ToDoAdapter(private val context: Context) : RecyclerView.Adapter<ToDoAdapter.ViewHolder>() {

    private val data: MutableList<ToDoModel> = mutableListOf()

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(model: ToDoModel) {
            
            view.toDoItem?.run {
                setOnCheckedChangeListener { _, isChecked ->
                    model.isCheck = isChecked
                }
                isChecked = model.isCheck
            }
            view.titleText?.run {
                text = model.title
                setTextColorByPriority(model.priority, this)
                setOnClickListener {
                    goToDetailActivity(model)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_to_do_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    fun add(item: ToDoModel) {
        this.data.add(item)
        notifyItemChanged(data.indexOf(item))
    }

    private fun setTextColorByPriority(priority: PriorityType, view: TextView) {
        when (priority) {
            PriorityType.HIGH -> {
                view.setTextColor(ContextCompat.getColor(context, R.color.red))
            }
            PriorityType.MEDIUM -> {
                view.setTextColor(ContextCompat.getColor(context, R.color.orange))
            }
            else -> {
                view.setTextColor(ContextCompat.getColor(context, R.color.green))
            }
        }
    }

    private fun goToDetailActivity(modelToDoModel: ToDoModel) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("title", modelToDoModel.title)
        intent.putExtra("description", modelToDoModel.description)
        intent.putExtra("priority", modelToDoModel.priority.toString())
        context.startActivity(intent)
    }
}
