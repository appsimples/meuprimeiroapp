package com.senec.outsmart.meuprimeiroapp.Model

/**
 * Created by outsmart on 13/08/2018.
 */
class ToDoModel(var title: String, var priority: PriorityType) {
    var description: String? = null
    var isCheck: Boolean = false
}