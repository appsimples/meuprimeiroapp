package com.senec.outsmart.meuprimeiroapp.Model

enum class PriorityType {
    HIGH,MEDIUM,LOW
}