package com.senec.outsmart.meuprimeiroapp.View

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.senec.outsmart.meuprimeiroapp.Controller.ToDoController
import com.senec.outsmart.meuprimeiroapp.Model.PriorityType
import com.senec.outsmart.meuprimeiroapp.R
import com.senec.outsmart.meuprimeiroapp.View.Adapter.ToDoAdapter
import kotlinx.android.synthetic.main.activity_to_do_list.*

class ToDoListActivity : BaseActivity() {

    private val recyclerViewAdapter = ToDoAdapter(context = this)
    private val toDoController by lazy { ToDoController() }
    private val requestCode = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_do_list)
        configRecyclerView()
        btnCreateTodo.setOnClickListener {
            startActivityForResult(Intent(this, CreateToDoActivity::class.java), requestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode && resultCode == Activity.RESULT_OK) {
            data?.let {
                val toDoModel = toDoController.createToDoModel(it.getStringExtra("title"), PriorityType.valueOf(it.getStringExtra("priority")), it.getStringExtra("description"))
                recyclerViewAdapter.add(toDoModel)
            }
        }
    }


    private fun configRecyclerView() {
        ToDoRecycler?.run {
            layoutManager = LinearLayoutManager(this@ToDoListActivity)
            adapter = recyclerViewAdapter
            isFocusable = false
            isNestedScrollingEnabled = false
        }
        //TODO: Remover Depois de Implementado o Real
        recyclerViewAdapter.add(toDoController.createMockToDoModel())
    }
}
